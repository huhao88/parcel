# -*- coding: utf8 -*-
from django.conf import settings

INIT_MSG = "\n\n查邮件查邮费，预约上门取包裹，甚至聊天解闷，我全会哦，快来试试吧！"

VIP_MSG = "\n\n请拨打079111185，会有客服美眉专门为你服务哒~"

HELP_MSG = "\n\n1) 邮件查询: 可以查询包括邮政小包（电商小包）、EMS、国内小包、普通包裹、挂号信等邮件；\n2) 意见投诉: 欢迎您对我们的服务进行投诉或提出您宝贵的意见和建议哦；\n3) 人工咨询: 能帮助您快速联系上我们的客服美眉哟；\n4) 帮助信息: 可以查看帮助信息，在输入框输入help或?，同样可以哟；\n5) 更多服务: 如需更多服务，欢迎您关注江西邮政11185客服微信号；"

TIP_MSG = "戳我，戳我，会有更多服务哦！"

def do_subscribe():
    reply = "亲~\n我是江西邮政电商小包公众号，欢迎关注我哟!" + HELP_MSG
    return reply

def do_other():
    reply = "hello"
    return reply

def do_help():
    reply = "【帮助信息】" + HELP_MSG
    return reply
    
def do_vip():
    reply = "【人工服务】" + VIP_MSG
    return reply
    
def do_fuwu():    
    reply = [
        ["【更多服务】",
         TIP_MSG,
        "%s/static/img/185_ewm.jpg"%(settings.WEIXIN_HOST,),
        "%s/more_serve"%(settings.WEIXIN_HOST,)
        ],
    ]
    return reply
