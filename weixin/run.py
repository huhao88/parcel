# -*- coding: utf8 -*-

import os, os.path
import re
import rules

def do_handler(message):
    found = False
    for i in rules.PATTERNS:
        regex = re.compile(i[0])
        m = regex.match(message)
        if m != None:
            found = True
            d = m.groupdict()
            pkg_name, mod_name, func_name = i[1].split(".")
            module = getattr(__import__(pkg_name+"."+mod_name), mod_name)
            func = getattr(module, func_name) 
            if d:
                reply = func(*m.groups())
            else:
                reply = func()
    if not found:
        reply = "请按格式输入消息"

    return reply
