# -*- coding: utf8 -*-

from django import template

register = template.Library()

@register.filter(name='status_date')
def status_date(date):
    message=''
    fgf='-'
    message=date[0:4]+fgf+date[4:6]+fgf+date[6:]
    return message

@register.filter(name='status_time')
def status_time(time):
    message=''
    fgf=':'
    message=time[0:2]+fgf+time[2:4]
    return message
