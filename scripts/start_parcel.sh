#!/bin/bash
. /home/huhao/testpy/bin/activate

PROJECT="/home/huhao/work/parcel"

cd $PROJECT

exec gunicorn parcel.wsgi:application -w 4
