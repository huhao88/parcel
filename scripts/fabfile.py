from __future__ import with_statement

from fabric.api import run, put, hosts, env, local, sudo

def staging():
    env.hosts = ['192.168.3.24']
    #env.hosts = ['172.31.254.71']
    env.user = 'huhao'
    env.password = '1234'

def stop():
    sudo("supervisorctl stop gunicorn")

def start():
    sudo("supervisorctl start gunicorn")

def deploy():
    local("tar zcvf jxepost.tar.gz jxepost --exclude='mysite.pid' --exclude='.git/*' --exclude='*.swp'")
    put("jxepost.tar.gz", "/home/huhao/work/parcel.tar.gz")
    run("cd /home/huhao/work/; rm -rf parcel")
    run("cd /home/huhao/work/; tar zxvf parcel.tar.gz") 
