# -*- coding: utf8 -*-

"""
Django settings for parcel project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'uy_@$=8_tn1*(@j&)cwq=l^)di#02&@)aq-2z9k2mf*!vgq!aa'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'weixin',
    'werobot',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'parcel.urls'

WSGI_APPLICATION = 'parcel.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
 
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'zh-CN'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = '/var/www/parcel/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

TEMPLATE_DIRS = ( 
    os.path.join(BASE_DIR, "templates"),
)

#ORACLE_NLS_LANG = 'SIMPLIFIED CHINESE_CHINA.UTF8'

TOKEN = "huhao"
APPID = "wx2880fb45790585c0"
SECRET = "a07c8ce5c3304c02381119777fe912ae"

WEIXIN_HOST = "http://111.75.223.89"

MENU_DATA = {
    "button": [ 

        {
            "type": "view", 
            "name": "我要查件", 
            "url": "%s/input_parcel"%(WEIXIN_HOST,)

        },
        {
            "name": "投诉咨询", 
            "sub_button": [
                {
                   "type": "view", 
                    "name": "邮费咨询", 
                    "url": "%s/postage"%(WEIXIN_HOST,)
                },
                {
                    "type": "view", 
                    "name": "意见投诉", 
                    "url": "http://yz.jxict.cn/WeChatOpen/business/openTousuPage.action "
                },
                {
                    "type": "click", 
                    "name": "人工服务", 
                    "key": "V-vip"
                } 
            ]

        },
        {
            "name": "更多服务", 
            "sub_button": [
                {
                    "type": "click", 
                    "name": "帮助信息", 
                    "key": "V-help"
                },
                {
                    "type": "click", 
                    "name": "更多服务", 
                    "key": "V-fuwu"
                }
            ]

        }
    ]
}
