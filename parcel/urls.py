from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'parcel.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url('^$', 'weixin.views.weixin'),
    url('^create_menu$', 'weixin.views.create_menu'),
    url('^input_parcel$', 'weixin.views.input_parcel'),
    url('^query_parcel$', 'weixin.views.query_parcel'),
    url('^more_serve$', 'weixin.views.more_serve'),
    url('^postage$', 'weixin.views.postage'),
)
